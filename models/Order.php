<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property string $no_faktur
 * @property string $tgl
 * @property string $kd_mhn
 * @property string $nm_mhn
 * @property string $almt_mhn
 * @property string $ds_mhn
 * @property string $kec_mhn
 * @property string $kab_mhn
 * @property string $kdpos_mhn
 * @property string $noktp_mhn
 * @property string $telp_mhn
 * @property string $kd_cust
 * @property string $nm_cust
 * @property string $almt_cust
 * @property string $ds_cust
 * @property string $kec_cust
 * @property string $kab_cust
 * @property string $kdpos_cust
 * @property string $noktp_cust
 * @property string $telp_cust
 * @property string $no_mesin
 * @property string $no_rangka
 * @property string $warna
 * @property string $warna_list
 * @property string $nm_mtr
 * @property string $type_mtr
 * @property string $tahun
 * @property int $hrg
 * @property int $um
 * @property int $tenor
 * @property int $cicilan
 * @property string $kd_bayar
 * @property string $no_pol
 * @property int $stnk_jadi
 * @property int $stnk_ambil
 * @property string $tgl_stnk
 * @property int $notis_jadi
 * @property int $plat_jadi
 * @property int $plat_ambil
 * @property int $bpkb_jadi
 * @property int $bpkb_ambil
 * @property string $no_bpkb
 * @property string $tgl_bpkb
 * @property string $no_stck
 * @property string $tgl_stck
 * @property string $nm_cmo
 * @property string $nm_sales
 * @property string $nm_admin
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_faktur'], 'required'],
            [['tgl', 'tgl_stnk', 'tgl_bpkb', 'tgl_stck'], 'safe'],
            [['hrg', 'um', 'tenor', 'cicilan', 'stnk_jadi', 'stnk_ambil', 'notis_jadi', 'plat_jadi', 'plat_ambil', 'bpkb_jadi', 'bpkb_ambil'], 'integer'],
            [['no_faktur', 'noktp_mhn', 'telp_mhn', 'noktp_cust', 'telp_cust', 'no_rangka', 'type_mtr'], 'string', 'max' => 20],
            [['kd_mhn', 'kd_cust', 'kd_bayar', 'no_pol', 'no_stck'], 'string', 'max' => 10],
            [['nm_mhn', 'nm_cust', 'nm_mtr', 'nm_cmo', 'nm_sales', 'nm_admin'], 'string', 'max' => 30],
            [['almt_mhn', 'almt_cust'], 'string', 'max' => 35],
            [['ds_mhn', 'kec_mhn', 'kab_mhn', 'ds_cust', 'kec_cust', 'kab_cust', 'warna', 'warna_list'], 'string', 'max' => 25],
            [['kdpos_mhn', 'kdpos_cust'], 'string', 'max' => 5],
            [['no_mesin', 'no_bpkb'], 'string', 'max' => 15],
            [['tahun'], 'string', 'max' => 4],
            [['no_faktur'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_faktur' => Yii::t('app', 'No Faktur'),
            'tgl' => Yii::t('app', 'Tgl'),
            'kd_mhn' => Yii::t('app', 'Kd Mhn'),
            'nm_mhn' => Yii::t('app', 'Nm Mhn'),
            'almt_mhn' => Yii::t('app', 'Almt Mhn'),
            'ds_mhn' => Yii::t('app', 'Ds Mhn'),
            'kec_mhn' => Yii::t('app', 'Kec Mhn'),
            'kab_mhn' => Yii::t('app', 'Kab Mhn'),
            'kdpos_mhn' => Yii::t('app', 'Kdpos Mhn'),
            'noktp_mhn' => Yii::t('app', 'Noktp Mhn'),
            'telp_mhn' => Yii::t('app', 'Telp Mhn'),
            'kd_cust' => Yii::t('app', 'Kd Cust'),
            'nm_cust' => Yii::t('app', 'Nm Cust'),
            'almt_cust' => Yii::t('app', 'Almt Cust'),
            'ds_cust' => Yii::t('app', 'Ds Cust'),
            'kec_cust' => Yii::t('app', 'Kec Cust'),
            'kab_cust' => Yii::t('app', 'Kab Cust'),
            'kdpos_cust' => Yii::t('app', 'Kdpos Cust'),
            'noktp_cust' => Yii::t('app', 'Noktp Cust'),
            'telp_cust' => Yii::t('app', 'Telp Cust'),
            'no_mesin' => Yii::t('app', 'No Mesin'),
            'no_rangka' => Yii::t('app', 'No Rangka'),
            'warna' => Yii::t('app', 'Warna'),
            'warna_list' => Yii::t('app', 'Warna List'),
            'nm_mtr' => Yii::t('app', 'Nm Mtr'),
            'type_mtr' => Yii::t('app', 'Type Mtr'),
            'tahun' => Yii::t('app', 'Tahun'),
            'hrg' => Yii::t('app', 'Hrg'),
            'um' => Yii::t('app', 'Um'),
            'tenor' => Yii::t('app', 'Tenor'),
            'cicilan' => Yii::t('app', 'Cicilan'),
            'kd_bayar' => Yii::t('app', 'Kd Bayar'),
            'no_pol' => Yii::t('app', 'No Pol'),
            'stnk_jadi' => Yii::t('app', 'Stnk Jadi'),
            'stnk_ambil' => Yii::t('app', 'Stnk Ambil'),
            'tgl_stnk' => Yii::t('app', 'Tgl Stnk'),
            'notis_jadi' => Yii::t('app', 'Notis Jadi'),
            'plat_jadi' => Yii::t('app', 'Plat Jadi'),
            'plat_ambil' => Yii::t('app', 'Plat Ambil'),
            'bpkb_jadi' => Yii::t('app', 'Bpkb Jadi'),
            'bpkb_ambil' => Yii::t('app', 'Bpkb Ambil'),
            'no_bpkb' => Yii::t('app', 'No Bpkb'),
            'tgl_bpkb' => Yii::t('app', 'Tgl Bpkb'),
            'no_stck' => Yii::t('app', 'No Stck'),
            'tgl_stck' => Yii::t('app', 'Tgl Stck'),
            'nm_cmo' => Yii::t('app', 'Nm Cmo'),
            'nm_sales' => Yii::t('app', 'Nm Sales'),
            'nm_admin' => Yii::t('app', 'Nm Admin'),
        ];
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
}
