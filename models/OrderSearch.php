<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form of `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_faktur', 'tgl', 'kd_mhn', 'nm_mhn', 'almt_mhn', 'ds_mhn', 'kec_mhn', 'kab_mhn', 'kdpos_mhn', 'noktp_mhn', 'telp_mhn', 'kd_cust', 'nm_cust', 'almt_cust', 'ds_cust', 'kec_cust', 'kab_cust', 'kdpos_cust', 'noktp_cust', 'telp_cust', 'no_mesin', 'no_rangka', 'warna', 'warna_list', 'nm_mtr', 'type_mtr', 'tahun', 'kd_bayar', 'no_pol', 'tgl_stnk', 'no_bpkb', 'tgl_bpkb', 'no_stck', 'tgl_stck', 'nm_cmo', 'nm_sales', 'nm_admin'], 'safe'],
            [['hrg', 'um', 'tenor', 'cicilan', 'stnk_jadi', 'stnk_ambil', 'notis_jadi', 'plat_jadi', 'plat_ambil', 'bpkb_jadi', 'bpkb_ambil'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
            'hrg' => $this->hrg,
            'um' => $this->um,
            'tenor' => $this->tenor,
            'cicilan' => $this->cicilan,
            'stnk_jadi' => $this->stnk_jadi,
            'stnk_ambil' => $this->stnk_ambil,
            'tgl_stnk' => $this->tgl_stnk,
            'notis_jadi' => $this->notis_jadi,
            'plat_jadi' => $this->plat_jadi,
            'plat_ambil' => $this->plat_ambil,
            'bpkb_jadi' => $this->bpkb_jadi,
            'bpkb_ambil' => $this->bpkb_ambil,
            'tgl_bpkb' => $this->tgl_bpkb,
            'tgl_stck' => $this->tgl_stck,
        ]);

        $query->andFilterWhere(['like', 'no_faktur', $this->no_faktur])
            ->andFilterWhere(['like', 'kd_mhn', $this->kd_mhn])
            ->andFilterWhere(['like', 'nm_mhn', $this->nm_mhn])
            ->andFilterWhere(['like', 'almt_mhn', $this->almt_mhn])
            ->andFilterWhere(['like', 'ds_mhn', $this->ds_mhn])
            ->andFilterWhere(['like', 'kec_mhn', $this->kec_mhn])
            ->andFilterWhere(['like', 'kab_mhn', $this->kab_mhn])
            ->andFilterWhere(['like', 'kdpos_mhn', $this->kdpos_mhn])
            ->andFilterWhere(['like', 'noktp_mhn', $this->noktp_mhn])
            ->andFilterWhere(['like', 'telp_mhn', $this->telp_mhn])
            ->andFilterWhere(['like', 'kd_cust', $this->kd_cust])
            ->andFilterWhere(['like', 'nm_cust', $this->nm_cust])
            ->andFilterWhere(['like', 'almt_cust', $this->almt_cust])
            ->andFilterWhere(['like', 'ds_cust', $this->ds_cust])
            ->andFilterWhere(['like', 'kec_cust', $this->kec_cust])
            ->andFilterWhere(['like', 'kab_cust', $this->kab_cust])
            ->andFilterWhere(['like', 'kdpos_cust', $this->kdpos_cust])
            ->andFilterWhere(['like', 'noktp_cust', $this->noktp_cust])
            ->andFilterWhere(['like', 'telp_cust', $this->telp_cust])
            ->andFilterWhere(['like', 'no_mesin', $this->no_mesin])
            ->andFilterWhere(['like', 'no_rangka', $this->no_rangka])
            ->andFilterWhere(['like', 'warna', $this->warna])
            ->andFilterWhere(['like', 'warna_list', $this->warna_list])
            ->andFilterWhere(['like', 'nm_mtr', $this->nm_mtr])
            ->andFilterWhere(['like', 'type_mtr', $this->type_mtr])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'kd_bayar', $this->kd_bayar])
            ->andFilterWhere(['like', 'no_pol', $this->no_pol])
            ->andFilterWhere(['like', 'no_bpkb', $this->no_bpkb])
            ->andFilterWhere(['like', 'no_stck', $this->no_stck])
            ->andFilterWhere(['like', 'nm_cmo', $this->nm_cmo])
            ->andFilterWhere(['like', 'nm_sales', $this->nm_sales])
            ->andFilterWhere(['like', 'nm_admin', $this->nm_admin]);

        return $dataProvider;
    }
}
