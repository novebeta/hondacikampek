<?php
namespace app\modules\api\models;
use Yii;
/**
 * This is the model class for table "{{%country}}".
 *
 * @property string $code
 * @property string $name
 * @property int $population
 */
class Country extends \yii\db\ActiveRecord {
//	const SCENARIO_CREATE = 'create';
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%country}}';
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[ [ 'code', 'name' ], 'required' ],
			[ [ 'population' ], 'integer' ],
			[ [ 'code' ], 'string', 'max' => 2 ],
			[ [ 'name' ], 'string', 'max' => 52 ],
			[ [ 'code' ], 'unique' ],
		];
	}
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'code'       => Yii::t( 'app', 'Code' ),
			'name'       => Yii::t( 'app', 'Name' ),
			'population' => Yii::t( 'app', 'Population' ),
		];
	}
//	public function scenarios() {
//		$scenarios           = parent::scenarios();
//		$scenarios['create'] = [ 'code', 'name', 'population' ];
//		return $scenarios;
//	}
	/**
	 * @inheritdoc
	 * @return CountryQuery the active query used by this AR class.
	 */
	public static function find() {
		return new CountryQuery( get_called_class() );
	}
}
