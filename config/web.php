<?php
$params = require __DIR__ . '/params.php';
$db     = require __DIR__ . '/db.php';
$config = [
	'id'         => 'basic',
	'basePath'   => dirname( __DIR__ ),
	'bootstrap'  => [ 'log' ],
	'aliases'    => [
		'@bower' => '@vendor/bower-asset',
		'@npm'   => '@vendor/npm-asset',
	],
	'modules'    => [
		'api' => [
			'basePath' => '@app/modules/api',
			'class'    => 'app\modules\api\Api',
		],
	],
	'components' => [
		'formatter'    => [
			'dateFormat'        => 'dd.MM.yyyy',
			'decimalSeparator'  => '.',
			'thousandSeparator' => ',',
			'currencyCode'      => 'Rp ',
		],
//        'assetManager' => [
//            'bundles' => [
//                'dmstr\web\AdminLteAsset' => [
//                    'skin' => 'skin-red',
//                ],
//            ],
//        ],
		'request'      => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'oG8O92TRFKJLN2kOeaDUIelgSmI4has1',
			'parsers'             => [
				'application/json' => 'yii\web\JsonParser',
			]
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'identityClass'   => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer'       => [
			'class'            => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => [ 'error', 'warning' ],
				],
			],
		],
		'db'           => $db,
		'urlManager'   => [
			'class'               => 'yii\web\UrlManager',
			'enablePrettyUrl'     => false,
			'showScriptName'      => false,
			'enableStrictParsing' => true,
			'rules'               => [
				[
					'class'      => 'yii\rest\UrlRule',
					'controller' => [ 'api/country' ],
					'pluralize'  => false
				],
//	            '<alias:upload|index|hasil|view>' => 'site/<alias>',
				'<controller:\w+>/<id:\d+>'              => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
				'<alias:\w+>'                            => 'site/<alias>',
			],
		],
	],
//	'as access'  => [
//		'class' => AccessControl::className(),//AccessControl::className(),
//		'rules' => [
//			[
//				'actions' => [ 'login', 'error','api/*' ],
//				'allow'   => true,
//			],
//			[
//				'actions' => [ 'logout', 'index', 'view', 'upload' ], // add all actions to take guest to login page
//				'allow'   => true,
//				'roles'   => [ '@' ],
//			],
//		],
//	],
	'params'     => $params,
];
if ( YII_ENV_DEV ) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]      = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
	$config['bootstrap'][]      = 'gii';
	$config['modules']['gii']   = [
		'class'      => 'yii\gii\Module',
		'allowedIPs' => [ '127.0.0.1', '::1' ],
		'generators' => [ //here
			'crud' => [
				'class'     => 'yii\gii\generators\crud\Generator',
				'templates' => [
					'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
				]
			]
		],
//        'allowedIPs' => ['127.0.0.1', '::1'],
	];
}
return $config;
