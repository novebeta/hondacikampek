<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
/* @var $this yii\web\View */
/* @var $model app\models\Order */
$this->title = $model->no_faktur;
?>
<div class="tabpencarian">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <fieldset>
                <legend>Faktur <?php echo $model->no_faktur; ?><? if ( $log !== null ) : ?>
                        <span class="badge">last update : <?= $log->created_at; ?></span> <? endif; ?></legend>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
			<?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
				<?= Tabs::widget( [
					'items' => [
						[
							'label'   => 'Data Faktur',
							'content' => $this->render( '_faktur', [ 'model' => $model, 'form' => $form ] ),
							'active'  => true
						],
						[
							'label'   => 'Data Pemohon/STNK',
							'content' => $this->render( '_pemohon', [ 'model' => $model, 'form' => $form ] ),
						],
//			        [
//				        'label' => 'Data STNK',
//				        'content' => $this->render('_stnk', ['model' => $model, 'form' => $form]),
//			        ],
						[
							'label'   => 'Data Kendaraan',
							'content' => $this->render( '_kendaraan', [ 'model' => $model, 'form' => $form ] ),
						]
					]
				] );
				?>
            </div>
			<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
