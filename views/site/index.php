<?php
/* @var $this \yii\web\View */
/* @var $searchModel \app\models\OrderSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
use yii\grid\GridView;
use yii\widgets\Pjax;
Pjax::begin(); ?>
    <div class="tabpencarian">
        <div class="row">
            <div class="col-md-12 col-sm-12">
				<?php echo $this->render( '_search', [
					'log' => $log
				] ); ?>
                <!-- Form Name -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
				<? if ( isset( $dataProvider ) ) {
					echo GridView::widget( [
						'dataProvider' => $dataProvider,
//						'filterModel'  => $searchModel,
						'columns'      => [
							[ 'class' => 'yii\grid\SerialColumn' ],
							'nm_cust',
							'tgl',
							'no_faktur',
							'nm_mtr',
							'type_mtr',
							'no_mesin',
							'no_rangka',
							[ 'class' => 'yii\grid\ActionColumn', 'template' => '{view}' ],
						],
					] );
				} ?>
            </div>
        </div>
    </div>
<?php Pjax::end(); ?>