<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin( [
	'action'  => [ 'index' ],
	'method'  => 'get',
	'options' => [
		'data-pjax' => 1,
		'class'     => 'form-horizontal'
	],
] ); ?>
<fieldset>
    <legend>Pencarian</legend>
    <!--    --><? //= $form->field($model, 'no_faktur') ?>
    <!--    --><? //= $form->field($model, 'tgl_faktur') ?>
    <!---->
    <!--    --><? //= $form->field($model, 'kd_mhn') ?>
    <!---->
    <!--    --><? //= $form->field($model, 'nm_mhn') ?>
    <!---->
    <!--    --><? //= $form->field($model, 'almt_mhn') ?>
    <!-- Select Basic -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="selectbasic">Pilihan</label>
        <div class="col-md-4">
            <select id="selectbasic" name="query_field" class="form-control">
                <option value="telp_cust">No. HP</option>
                <option value="noktp_cust">No. KTP</option>
                <option value="no_pol">No. Polisi</option>
                <option value="no_rangka">No. Rangka</option>
                <option value="no_mesin">No. Mesin</option>
                <option value="nm_cust">Nama</option>
            </select>
        </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="Input">Input</label>
        <div class="col-md-4">
            <input id="Input" name="query_value" type="text" placeholder="masukkan data" class="form-control input-md">
        </div>
    </div>
    <?php if($log !== null) : ?>
    <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
            <small><label class="control-label"><span class="badge">Last Update : <?= $log->created_at; ?></span></label></small>
        </div>
    </div>
    <? endif;?>
	<?php // echo $form->field($model, 'ds_mhn') ?>

	<?php // echo $form->field($model, 'kec_mhn') ?>

	<?php // echo $form->field($model, 'kdpos_mhn') ?>
    <!--	--><?php //echo $form->field( $model, 'noktp_mhn' ) ?>
    <!--	--><?php //echo $form->field( $model, 'telp_mhn' ) ?>
	<?php // echo $form->field($model, 'kd_cust') ?>

	<?php // echo $form->field($model, 'nm_cust') ?>

	<?php // echo $form->field($model, 'almt_cust') ?>

	<?php // echo $form->field($model, 'ds_cust') ?>

	<?php // echo $form->field($model, 'kec_cust') ?>

	<?php // echo $form->field($model, 'kdpos_cust') ?>

	<?php // echo $form->field($model, 'noktp_cust') ?>

	<?php // echo $form->field($model, 'telp_cust') ?>

	<?php // echo $form->field($model, 'no_mesin') ?>

	<?php // echo $form->field($model, 'no_rangka') ?>

	<?php // echo $form->field($model, 'warna') ?>

	<?php // echo $form->field($model, 'warna_list') ?>

	<?php // echo $form->field($model, 'type_mtr') ?>

	<?php // echo $form->field($model, 'tahun') ?>

	<?php // echo $form->field($model, 'hrg') ?>

	<?php // echo $form->field($model, 'um') ?>

	<?php // echo $form->field($model, 'tenor') ?>

	<?php // echo $form->field($model, 'cicilan') ?>

	<?php // echo $form->field($model, 'kd_bayar') ?>

	<?php // echo $form->field($model, 'no_pol') ?>

	<?php // echo $form->field($model, 'stnk_jadi') ?>

	<?php // echo $form->field($model, 'tgl_stnk') ?>

	<?php // echo $form->field($model, 'notis_jadi') ?>

	<?php // echo $form->field($model, 'plat_jadi') ?>

	<?php // echo $form->field($model, 'bpkb_jadi') ?>

	<?php // echo $form->field($model, 'no_bpkb') ?>

	<?php // echo $form->field($model, 'tgl_bpkb') ?>

	<?php // echo $form->field($model, 'no_stck') ?>

	<?php // echo $form->field($model, 'tgl_stck') ?>

	<?php // echo $form->field($model, 'nm_cmo') ?>

	<?php // echo $form->field($model, 'nm_sales') ?>

	<?php // echo $form->field($model, 'nm_admin') ?>
    <div class="form-group">
        <label class="col-md-4 control-label" for="singlebutton"></label>
        <div class="col-md-4">
			<?= Html::submitButton( Yii::t( 'app', 'Search' ), [ 'class' => 'btn btn-primary' ] ) ?>
			<?= Html::resetButton( Yii::t( 'app', 'Reset' ), [ 'class' => 'btn btn-default' ] ) ?>
        </div>
    </div>
</fieldset>
<?php ActiveForm::end(); ?>
