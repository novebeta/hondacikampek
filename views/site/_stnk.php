<div class="form-group">
	<?= $form->field( $model, 'nm_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'almt_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'ds_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kab_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kec_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kdpos_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'noktp_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'telp_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
