<div class="tabpencarian">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <fieldset>
                <legend>Upload Document</legend>
            </fieldset>
            <!--                <label class='col-md-4 control-label for="_data_"'>File : (*.txt)</label>-->
            <div class="col-md-6">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Keterangan</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach ( $hasil_upload as $key => $row ) : ?>
                        <tr>
                            <td><?= $key ?></td>
                            <td><?= $row ?></td>
                        </tr>
					<?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>