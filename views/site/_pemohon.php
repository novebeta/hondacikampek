



<div class="form-group">
	<?= $form->field( $model, 'nm_mhn' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'almt_mhn' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'ds_mhn' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kec_mhn' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kab_mhn' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kdpos_mhn' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'noktp_mhn' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'telp_mhn' )->textInput( [ 'disabled' => true ] ) ?>
</div>

<div class="form-group">
	<?= $form->field( $model, 'nm_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'almt_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'ds_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kec_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kab_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kdpos_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'noktp_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'telp_cust' )->textInput( [ 'disabled' => true ] ) ?>
</div>


<div class="form-group">
	<?= $form->field( $model, 'nm_cmo' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'nm_sales' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'nm_admin' )->textInput( [ 'disabled' => true ] ) ?>
</div>