<?php
/* @var $form static */
/* @var $this \yii\web\View */
/* @var $model \app\models\Order|\yii\db\ActiveRecord */
?>
<div class="form-group">
	<?= $form->field( $model, 'nm_mtr' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'no_mesin' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'no_rangka' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'warna' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'warna_list' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'type_mtr' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'tahun' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'hrg', [
		'inputOptions' => [ 'value' => Yii::$app->formatter->asCurrency( $model->hrg ) ]
	] )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'um', [
		'inputOptions' => [ 'value' => Yii::$app->formatter->asCurrency( $model->um ) ]
	] )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'tenor' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'cicilan', [
		'inputOptions' => [ 'value' => Yii::$app->formatter->asCurrency( $model->cicilan ) ]
	] )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'kd_bayar' )->textInput( [ 'disabled' => true ] ) ?>
</div>