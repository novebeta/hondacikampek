<div class="tabpencarian">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <fieldset>
                <legend>Upload Document</legend>
            </fieldset>
            <!--                <label class='col-md-4 control-label for="_data_"'>File : (*.txt)</label>-->
            <div class="col-md-6">
				<?php
				use yii\bootstrap\ActiveForm;
				use yii\bootstrap\Html;
				$form = ActiveForm::begin( [
					'options' => [ 'enctype' => 'multipart/form-data' ]
				] ); // important
				?>
                <div class="form-group">
					<?php
					echo \kartik\file\FileInput::widget( [
						'name'          => '_data_',
						'pluginOptions' => [
							'showPreview'           => false,
//							'uploadUrl' => yii\helpers\Url::to( [ '/site/upload' ] )
							'allowedFileExtensions' => [ 'sm' ],
							'showUpload'            => false
						]
					] );
					?>
                </div>
                <div class="form-group">
					<?= Html::submitButton( Yii::t( 'app', 'Upload' ), [ 'class' => 'btn btn-success' ] ) ?>
                </div>
				<?php
				ActiveForm::end();
				?>
            </div>
        </div>
    </div>
</div>