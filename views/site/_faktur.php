<div class="form-group">
	<?= $form->field( $model, 'no_faktur' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'tgl', [
		'inputOptions' => [
			'value' => ( $model->tgl == '0000-00-00' ) ? '' :
				Yii::$app->formatter->asDate( $model->tgl )
		]
	] )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'no_pol' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-xs-3">
            <label class="control-label">STNK</label>
        </div>
        <div class="col-xs-3">
			<?= $form->field( $model, 'stnk_jadi' )->checkbox( [ 'disabled' => true ] ) ?>
        </div>
        <div class="col-xs-3">
			<?= $form->field( $model, 'stnk_ambil' )->checkbox( [ 'disabled' => true ] ) ?>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-xs-3">
            <label class="control-label">NOTIS</label>
        </div>
        <div class="col-xs-3">
			<?= $form->field( $model, 'notis_jadi' )->checkbox( [ 'disabled' => true ] ) ?>
        </div>
        <div class="col-xs-3">
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-xs-3">
            <label class="control-label">PLAT</label>
        </div>
        <div class="col-xs-3">
			<?= $form->field( $model, 'plat_jadi' )->checkbox( [ 'disabled' => true ] ) ?>
        </div>
        <div class="col-xs-3">
			<?= $form->field( $model, 'plat_ambil' )->checkbox( [ 'disabled' => true ] ) ?>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-xs-3">
            <label class="control-label">BPKB</label>
        </div>
        <div class="col-xs-3">
			<?= $form->field( $model, 'bpkb_jadi' )->checkbox( [ 'disabled' => true ] ) ?>
        </div>
        <div class="col-xs-3">
			<?= $form->field( $model, 'bpkb_ambil' )->checkbox( [ 'disabled' => true ] ) ?>
        </div>
    </div>
</div>
<div class="form-group">
	<?= $form->field( $model, 'tgl_stnk', [
		'inputOptions' => [
			'value' => ( $model->tgl_stnk == '0000-00-00' ) ? '' :
				Yii::$app->formatter->asDate( $model->tgl_stnk )
		]
	] )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'no_bpkb' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'tgl_bpkb', [
		'inputOptions' => [
			'value' => ( $model->tgl_bpkb == '0000-00-00' ) ? '' :
				Yii::$app->formatter->asDate( $model->tgl_bpkb )
		]
	] )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'no_stck' )->textInput( [ 'disabled' => true ] ) ?>
</div>
<div class="form-group">
	<?= $form->field( $model, 'tgl_stck', [
		'inputOptions' => [
			'value' => ( $model->tgl_stck == '0000-00-00' ) ? '' :
				Yii::$app->formatter->asDate( $model->tgl_stck )
		]
	] )->textInput( [ 'disabled' => true ] ) ?>
</div>