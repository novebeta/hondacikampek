<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $model->no_faktur;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->no_faktur], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->no_faktur], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no_faktur',
            'tgl_faktur',
            'kd_mhn',
            'nm_mhn',
            'almt_mhn',
            'ds_mhn',
            'kec_mhn',
            'kdpos_mhn',
            'noktp_mhn',
            'telp_mhn',
            'kd_cust',
            'nm_cust',
            'almt_cust',
            'ds_cust',
            'kec_cust',
            'kdpos_cust',
            'noktp_cust',
            'telp_cust',
            'no_mesin',
            'no_rangka',
            'warna',
            'warna_list',
            'type_mtr',
            'tahun',
            'hrg',
            'um',
            'tenor',
            'cicilan',
            'kd_bayar',
            'no_pol',
            'stnk_jadi',
            'tgl_stnk',
            'notis_jadi',
            'plat_jadi',
            'bpkb_jadi',
            'no_bpkb',
            'tgl_bpkb',
            'no_stck',
            'tgl_stck',
            'nm_cmo',
            'nm_sales',
            'nm_admin',
        ],
    ]) ?>

</div>
