<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_faktur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl_faktur')->textInput() ?>

    <?= $form->field($model, 'kd_mhn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nm_mhn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'almt_mhn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ds_mhn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kec_mhn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kdpos_mhn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'noktp_mhn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telp_mhn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kd_cust')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nm_cust')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'almt_cust')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ds_cust')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kec_cust')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kdpos_cust')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'noktp_cust')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telp_cust')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_mesin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_rangka')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warna')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warna_list')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_mtr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hrg')->textInput() ?>

    <?= $form->field($model, 'um')->textInput() ?>

    <?= $form->field($model, 'tenor')->textInput() ?>

    <?= $form->field($model, 'cicilan')->textInput() ?>

    <?= $form->field($model, 'kd_bayar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_pol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stnk_jadi')->textInput() ?>

    <?= $form->field($model, 'tgl_stnk')->textInput() ?>

    <?= $form->field($model, 'notis_jadi')->textInput() ?>

    <?= $form->field($model, 'plat_jadi')->textInput() ?>

    <?= $form->field($model, 'bpkb_jadi')->textInput() ?>

    <?= $form->field($model, 'no_bpkb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl_bpkb')->textInput() ?>

    <?= $form->field($model, 'no_stck')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl_stck')->textInput() ?>

    <?= $form->field($model, 'nm_cmo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nm_sales')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nm_admin')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
