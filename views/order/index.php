<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Order'), ['create'], ['class' => 'btn btn-success ahm-def-btn']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'no_faktur',
//            'tgl_faktur',
            'kd_mhn',
            'nm_mhn',
            'almt_mhn',
            //'ds_mhn',
            //'kec_mhn',
            //'kdpos_mhn',
            //'noktp_mhn',
            //'telp_mhn',
            //'kd_cust',
            //'nm_cust',
            //'almt_cust',
            //'ds_cust',
            //'kec_cust',
            //'kdpos_cust',
            //'noktp_cust',
            //'telp_cust',
            //'no_mesin',
            //'no_rangka',
            //'warna',
            //'warna_list',
            //'type_mtr',
            //'tahun',
            //'hrg',
            //'um',
            //'tenor',
            //'cicilan',
            //'kd_bayar',
            //'no_pol',
            //'stnk_jadi',
            //'tgl_stnk',
            //'notis_jadi',
            //'plat_jadi',
            //'bpkb_jadi',
            //'no_bpkb',
            //'tgl_bpkb',
            //'no_stck',
            //'tgl_stck',
            //'nm_cmo',
            //'nm_sales',
            //'nm_admin',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
