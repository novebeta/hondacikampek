<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'no_faktur') ?>

    <?= $form->field($model, 'tgl_faktur') ?>

    <?= $form->field($model, 'kd_mhn') ?>

    <?= $form->field($model, 'nm_mhn') ?>

    <?= $form->field($model, 'almt_mhn') ?>

    <?php // echo $form->field($model, 'ds_mhn') ?>

    <?php // echo $form->field($model, 'kec_mhn') ?>

    <?php // echo $form->field($model, 'kdpos_mhn') ?>

    <?php // echo $form->field($model, 'noktp_mhn') ?>

    <?php // echo $form->field($model, 'telp_mhn') ?>

    <?php // echo $form->field($model, 'kd_cust') ?>

    <?php // echo $form->field($model, 'nm_cust') ?>

    <?php // echo $form->field($model, 'almt_cust') ?>

    <?php // echo $form->field($model, 'ds_cust') ?>

    <?php // echo $form->field($model, 'kec_cust') ?>

    <?php // echo $form->field($model, 'kdpos_cust') ?>

    <?php // echo $form->field($model, 'noktp_cust') ?>

    <?php // echo $form->field($model, 'telp_cust') ?>

    <?php // echo $form->field($model, 'no_mesin') ?>

    <?php // echo $form->field($model, 'no_rangka') ?>

    <?php // echo $form->field($model, 'warna') ?>

    <?php // echo $form->field($model, 'warna_list') ?>

    <?php // echo $form->field($model, 'type_mtr') ?>

    <?php // echo $form->field($model, 'tahun') ?>

    <?php // echo $form->field($model, 'hrg') ?>

    <?php // echo $form->field($model, 'um') ?>

    <?php // echo $form->field($model, 'tenor') ?>

    <?php // echo $form->field($model, 'cicilan') ?>

    <?php // echo $form->field($model, 'kd_bayar') ?>

    <?php // echo $form->field($model, 'no_pol') ?>

    <?php // echo $form->field($model, 'stnk_jadi') ?>

    <?php // echo $form->field($model, 'tgl_stnk') ?>

    <?php // echo $form->field($model, 'notis_jadi') ?>

    <?php // echo $form->field($model, 'plat_jadi') ?>

    <?php // echo $form->field($model, 'bpkb_jadi') ?>

    <?php // echo $form->field($model, 'no_bpkb') ?>

    <?php // echo $form->field($model, 'tgl_bpkb') ?>

    <?php // echo $form->field($model, 'no_stck') ?>

    <?php // echo $form->field($model, 'tgl_stck') ?>

    <?php // echo $form->field($model, 'nm_cmo') ?>

    <?php // echo $form->field($model, 'nm_sales') ?>

    <?php // echo $form->field($model, 'nm_admin') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
