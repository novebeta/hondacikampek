<?php
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
?>
<header>
    <div class="logoWrapper">
        <div class="widthBox fullBox">
            <div class="hondaLogo">
                <a href="<?php Url::home( true ); ?>">
                    <img src="<?= Url::base(); ?>/images/rsz_favicon.png"
                         alt="Sepeda Motor Honda Terbaru by Star II"
                    >
                </a>
            </div>
            <div class="ahmLogo">
                <a href="<?= Url::home( true ); ?>">
                    <img src="<?= Url::base(); ?>/images/img_logo_honda.png" alt="Ahass">
                </a>
            </div>
        </div>
    </div>
</header>
<nav>
    <div class="widthBox fullBox">
        <a href="#!" class="mobile-brand"><img src="<?= Url::base(); ?>/images/home.png"></a>
        <div id="mobnav-btn">
            Menu<span class="fa fa-reorder"></span>
        </div>
        <div class="leftNav">
            <ul class="menu fullBox">
                <li>
                    <a href="<?= Url::home( true ); ?>">
                        <img class="imgx" src="<?= Url::base(); ?>/images/home.png">
                        <span>home</span>
                    </a>
                </li>
				<?php if ( ! Yii::$app->user->isGuest ) : ?>
                    <li>
                        <a href="<?= Url::toRoute( 'site/upload' ); ?>">
                            <span>Upload</span>
                        </a>
                    </li>
				<?php endif; ?>
                <!--                <li>-->
                <!--                    <a href="" >-->
                <!--                        <span>TENTANG KAMI</span>-->
                <!--                    </a>-->
                <!--                    <ul class="subMenu">-->
                <!--                        <li>-->
                <!--                            <a href="" >-->
                <!--                                PROFIL-->
                <!--                            </a></li>-->
                <!--                        <li>-->
                <!--                            <a href="" >-->
                <!--                                VISI MISI-->
                <!--                            </a></li>-->
                <!--                        <li>-->
                <!--                            <a href="" >-->
                <!--                                SEJARAH-->
                <!--                            </a></li>-->
                <!---->
                <!--                    </ul></li>-->
                <!--                <li>-->
                <!--                    <a href="" >-->
                <!--                        <span>PRODUK</span>-->
                <!--                    </a>-->
                <!--                </li>-->
                <!---->
                <!--                <li>-->
                <!--                    <a href="" >-->
                <!--                        <span>Cari Motor</span>-->
                <!--                    </a>-->
                <!--                </li>-->
                <!--                <li>-->
                <!--                    <a href="" >-->
                <!--                        <span>Simulasi Pembiayaan</span>-->
                <!--                    </a>-->
                <!--                </li>-->
                <!--                <li>-->
                <!--                    <a href="" >-->
                <!--                        <span>Kontak</span>-->
                <!--                    </a>-->
                <!--                </li>-->
				<?php if ( ! Yii::$app->user->isGuest ) : ?>
                    <li>
                        <a href="<?= Url::toRoute( 'site/logout' ) ?>">
                            <span>Logout</span>
                        </a>
                    </li>
				<?php endif; ?>
            </ul>
        </div>
        <!--        <div class="rightNav">-->
        <!--            <a href="javascript:void(0)" class="search">-->
        <!--                <form method="GET" action="http://www.astra-honda.com" accept-charset="UTF-8">-->
        <!--                    <input type="text" name="s" class="searchInput"><i class="fa fa-search" aria-hidden="true"></i>-->
        <!--                    <input style="display:none;" type="submit">-->
        <!--                </form>-->
        <!--            </a>-->
        <!--        </div>-->
    </div>
</nav>
