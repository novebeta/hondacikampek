<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
app\assets\AppAsset::register( $this );
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?=Url::base();?>/images/favicon.png">
    <link rel="stylesheet" id="my-font" href="http://fonts.googleapis.com/css?family=Oswald" type="text/css" media="all">

    <!--    <link rel="shortcut icon" type="image/x-icon" href="--><? //=$directoryAsset?><!--/img/favicon.png">-->
	<?= Html::csrfMetaTags() ?>
    <title><?= Html::encode( $this->title ) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
	<?= $this->render(
		'header.php'
//		['directoryAsset' => $directoryAsset]
	) ?>

	<?= $this->render(
		'content.php',
		[
			'content'        => $content,
//			'directoryAsset' => $directoryAsset
		]
	) ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
