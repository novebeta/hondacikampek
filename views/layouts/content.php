<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<section>
    <div class="widthBox">
	    <?= $content ?>
</section>
<!--<div class="siteMap">-->
<!--    <div class="widthBox">-->
<!--        <div class="panelWrapper fullBox">-->
<!--            <div class="sitemapBox fullBox">-->
<!--                <div class="panelx">-->
<!--                    <div class="box">-->
<!--                        <div class="heading">-->
<!--                            <h3>Profil</h3>-->
<!--                        </div>-->
<!--                        <ul class="list">-->
<!--                            <li><a href="" >Profile</a></li>-->
<!--                            <li><a href="" >Visi Misi</a></li>-->
<!--                            <li><a href="" >ONE HEART</a></li>-->
<!--                            <li><a href="" >Sejarah</a></li>-->
<!--                            <li><a href="" >Foto</a></li>-->
<!--                        </ul></div></div>-->
<!--                <div class="panelx">-->
<!--                    <div class="box">-->
<!--                        <div class="heading">-->
<!--                            <h3>Produk</h3>-->
<!--                        </div>-->
<!--                        <ul class="list">-->
<!--                            <li><a href="" >Honda Matik</a></li>-->
<!--                            <li><a href="" >Honda Bebek</a></li>-->
<!--                            <li><a href="" >Honda Sport</a></li>-->
<!--                            <li><a href="" >Price List</a></li>-->
<!--                        </ul></div></div>-->
<!--                <div class="panelx">-->
<!--                    <div class="box">-->
<!--                        <div class="heading">-->
<!--                            <h3>Layanan</h3>-->
<!--                        </div>-->
<!--                        <ul class="list">-->
<!--                            <li><a href="" >Penjualan</a></li>-->
<!--                            <li><a href="" >Booking Service</a></li>-->
<!--                            <li><a href="" >Service Ditempat</a></li>-->
<!--                            <li><a href="" >Service Darurat</a></li>-->
<!--                        </ul></div></div>-->
<!--                <div class="panelx">-->
<!--                    <div class="box">-->
<!--                        <div class="heading">-->
<!--                            <h3>Jam kerja</h3>-->
<!--                        </div>-->
<!--                        <ul class="list">-->
<!--                            <li><a href="" >Senin - Jum'at	08:00 - 16:30</a></li>-->
<!--                            <li><a href="" >Sabtu	08:00 - 15:00</a></li>-->
<!--                            <li><a href="" >Minggu	09:00 - 15:00</a></li>-->
<!--                        </ul></div></div>-->
<!---->
<!---->
<!--            </div>-->
<!--            <div class="customerPanel">-->
<!--                <div class="box">-->
<!--                    <div class="heading">-->
<!--                        <h3>customer care</h3>-->
<!--                    </div>-->
<!---->
<!--                    <div class="custcareCaption">-->
<!--                        <p>PD. Star Motor II <br /> Jl. A. Yani No.27, Cikampek<br /> Tel. </p>-->
<!--                    </div>-->
<!---->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<footer>-->
<!--    <p>&copy; Copyright 2018, <a href="" title="">Ahass Honda Motor</a>. All Rights Reserved.</p>-->
<!--</footer>      <a href="#" class="backtopScroll"></a>-->
<!--</div>-->