<?php
namespace app\controllers;
use app\models\ContactForm;
use app\models\Log;
use app\models\LoginForm;
use app\models\Order;
use app\models\OrderSearch;
use Yii;
use yii\bootstrap\Html;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
class SiteController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access'  => [
				'class' => AccessControl::className(),//AccessControl::className(),
				'only' => ['login', 'error', 'logout', 'index', 'view', 'upload'],
				'rules' => [
					[
						'actions' => [ 'login', 'error' ],
						'allow'   => true,
					],
					[
						'actions' => [ 'logout', 'index', 'view', 'upload' ], // add all actions to take guest to login page
						'allow'   => true,
						'roles'   => [ '@' ],
					],
				],
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
//			'auth' => [
//				'class'           => 'yii\authclient\AuthAction',
//				'successCallback' => [ $this, 'successCallback' ],
//				'successUrl'      => $this->successUrl
//			],
		];
	}
	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex() {
		$searchModel = new OrderSearch();
		$log         = Log::find()->orderBy( 'created_at DESC' )->one();

		if ( count( Yii::$app->request->queryParams ) > 0 ) {
			$get                 = Yii::$app->request->queryParams;
			$find['OrderSearch'] = [
				$get['query_field'] => $get['query_value']
			];
			$dataProvider        = $searchModel->search( $find );
			if ( $dataProvider->getTotalCount() == 1 ) {
				return $this->redirect( [ 'site/view', 'id' => $dataProvider->query->one()->no_faktur ] );
			}
			return $this->render( 'index', [
//				'searchModel'  => $searchModel,
				'dataProvider' => $dataProvider,
				'log'          => $log
			] );
		} else {
//			echo 'test';
//			die('1');
			return $this->render( 'index', [
//				'searchModel'  => $searchModel,
				'dataProvider' => null,
				'log'          => $log
			] );
		}
	}
	/**
	 * Displays homepage.
	 *
	 * @return string
	 * @throws \yii\db\Exception
	 */
	public function actionUpload() {
		if ( Yii::$app->request->post() ) {
			set_time_limit(0);
			$image   = UploadedFile::getInstanceByName( '_data_' );
			$dirName = Yii::getAlias( '@runtime' );
			$flname  = tempnam( $dirName, '_data_' );
			$image->saveAs( $flname );
//		    $uploadPath = $flname;
//		    $namafile = $post['biodata_id'] . ".jpg";
			$_data_ = file( $flname );
			$key    = [
				'no_faktur',
				'tgl',
				'kd_mhn',
				'nm_mhn',
				'almt_mhn',
				'ds_mhn',
				'kec_mhn',
				'kab_mhn',
				'kdpos_mhn',
				'noktp_mhn',
				'telp_mhn',
				'kd_cust',
				'nm_cust',
				'almt_cust',
				'ds_cust',
				'kec_cust',
				'kab_cust',
				'kdpos_cust',
				'noktp_cust',
				'telp_cust',
				'no_mesin',
				'no_rangka',
				'warna',
				'warna_list',
				'nm_mtr',
				'type_mtr',
				'tahun',
				'hrg',
				'um',
				'tenor',
				'cicilan',
				'kd_bayar',
				'no_pol',
				'stnk_jadi',
				'stnk_ambil',
				'tgl_stnk',
				'notis_jadi',
				'plat_jadi',
				'plat_ambil',
				'bpkb_jadi',
				'bpkb_ambil',
				'no_bpkb',
				'tgl_bpkb',
				'no_stck',
				'tgl_stck',
				'nm_cmo',
				'nm_sales',
				'nm_admin'
			];
			$hasil  = [];
			foreach ( $_data_ as $line ) {
				$value = explode( '#', $line );
//				$attrib = [];
				$order = Order::find()->where( [
					'no_faktur' => $value[0]
				] )->one();
				if ( $order == null ) {
					$order = new Order();
				}
				for ( $i = 0; $i < count( $key ); $i ++ ) {
					$order->setAttribute( $key[ $i ], $value[ $i ] );
				}
				if ( $order->save() ) {
					$hasil[ $order->no_faktur ] = '<p class="text-success">Berhasil diupload</p>';
					$log                        = new Log;
					$command                    = Yii::$app->getDb()
					                                       ->createCommand( "SELECT UUID();" );
					$log->log_id                = $command->queryScalar();
					$log->no_faktur             = $order->no_faktur;
					$log->created_at            = new Expression( 'now()' );
					if ( ! $log->save() ) {
						Yii::error( print_r( $log->getErrorSummary( true ) ) );
					}
				} else {
					$hasil[ $order->no_faktur ] = '<p class="text-danger">Gagal diupload. ' . Html::errorSummary( $order ) . '</p>';
				}
			}
			return $this->render( 'hasil', [
				'hasil_upload' => $hasil
			] );
		}
		return $this->render( 'upload' );
	}
	/**
	 * Displays a single Order model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
			'log'   => Log::find()->where( [ 'no_faktur' => $id ] )->orderBy( 'created_at DESC' )->one()
		] );
	}
	/**
	 * Login action.
	 *
	 * @return string
	 */
	public function actionLogin() {
		if ( ! Yii::$app->user->isGuest ) {
			return $this->goHome();
		}
		$model = new LoginForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->login() ) {
			return $this->goBack();
		}
//		$this->layout = 'main-login';
		return $this->render( 'login', [
			'model' => $model,
		] );
	}
	/**
	 * Logout action.
	 *
	 * @return Response
	 */
	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}
	/**
	 * Displays contact page.
	 *
	 * @return Response|string
	 */
	public function actionContact() {
		$model = new ContactForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->contact( Yii::$app->params['adminEmail'] ) ) {
			Yii::$app->session->setFlash( 'contactFormSubmitted' );
			return $this->refresh();
		}
		return $this->render( 'contact', [
			'model' => $model,
		] );
	}
	/**
	 * Displays about page.
	 *
	 * @return string
	 */
	public function actionAbout() {
		return $this->render( 'about' );
	}
	/**
	 * Finds the Order model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Order the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Order::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( Yii::t( 'app', 'The requested page does not exist.' ) );
	}
}
