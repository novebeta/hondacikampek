$(document).ready(function(){
	var hash = window.location.hash;
	// now scroll to element with that id
	//window.scrollTo(0, 0); // values are x,y-offset
	$('.subMenu li a').click(function(){
		var hasVal = $(this).attr('href');
		getHash = hasVal.split('#');
		//ini buat animasi scrollnya//
		$('html, body').animate({ scrollTop: $('#'+getHash[1]).offset().top - 50}, 900);
	});
	
	$('.mobile-topmenu').click(function(){
		$('.leftMenu').toggleClass('open');
	});
	$('#mobnav-btn').click(function(){
		$('.leftNav').toggleClass('open');
		$('.rightNav').toggleClass('open');
	});
	$('.menu li').each(function(){
		if($(this).children('ul').length > 0){
			$(this).addClass('hassubMenu');
		}
	});
	if ($(window).width() > 768) {
		$('.hassubMenu').hover(function(){
			$(this).addClass('open');
			return false;
		},function(){
			$(this).removeClass('open');
			return false;
		});
	}
	if ($(window).width() < 768) {
		$('.hassubMenu a').click(function(){
			$(this).parent().toggleClass('open');
			return false;
		});
	}
	$('.menu li a').each(function(){
		if($(this).find('span').css('display') == 'none'){
			$(this).find('img').css('padding-right', 0);
		}
	});
	
	$('.search').click(function(){
		$('.searchInput').css({
			padding: '7px 10px',
			width: 150
		});
		$('.searchInput').focus();
	});
	
	$('.searchInput').blur(function(){
		$('.searchInput').css({
			padding: 0,
			width: 0
		});
	});
	
	$('.backtopScroll').click(function(){
		$('html, body').animate({scrollTop: 0}, 500)
	});
	
	
	if($(".owl-carousel").length > 0) {
		$('.owl-carousel').each(function(){
			var item_count = parseInt($(this).find('div').length);
			if(item_count > 1){
				if($(this).hasClass('slider')){
					$(this).owlCarousel({
						items:1,
						autoplay:true,
						autoplayHoverPause:true,
						loop:true,
						nav:true,
						navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
					});
				}
				else if($(this).hasClass('number')){
					$(this).owlCarousel({
						items:1,
						autoplay:true,
						autoplayHoverPause:true,
						loop:true,
						nav:true,
						navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
						dotsData:true
					});
				}
				else if($(this).hasClass('new-content-element-slider')){
					$(this).owlCarousel({
						items:4,
						autoplay:true,
						autoplayHoverPause:true,
						nav:true,
						navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
						dots:false,
						margin:30
					});
				}
			}
		});
		
	}
	if($('.productRecBox').length>3){
	$('.productRec').owlCarousel({
		autoplay:true,
		autoplayHoverPause:true,
		loop:true,
		nav:true,
		dots:false,
		navText: ["<img src='http://cdnhonda.azureedge.net/templates/images/icon_slidernav_prev_redarrow.png'>","<img src='http://cdnhonda.azureedge.net/templates/images/icon_slidernav_next_redarrow.png'>"],
		responsive:{
			0:{
				items:1
			},
			640:{
				items:3
			}
		}
	});
	}

	if($(".featureSlider").length > 0) {
		$('.featureSlider').bxSlider({
			mode:'fade',
			auto: true,
			autoHover: true,
			prevText:'',
			nextText:'',
			speed: 800
		});
	}
	
	if($(".productWrapper .owl-carousel.mobile .owl-item").length < 1) {
		$(".productWrapper .owl-carousel.mobile").css('display', 'none');
		$(".productWrapper .owl-carousel.desktop").css('display', 'block');
	}

	if ($(".specTabs").length > 0){
		$(".specTabs").tabs();
	}
	
	if ($(window).width() > 768) {
		$('.toolTip').hover(function(){
			$(this).siblings('.partFeature').toggle();
		});
	} else {
		$('.toolTip').click(function(){
			$(this).siblings('.partFeature').show();
		});
		$('.closeBtn').click(function(){
			$('.partFeature').hide();
			return false;
		});
	}
	
	$('.poiPoint').each(function(){
		$(this).css('top', ($(this).data('y')*100)+'%');
		$(this).css('left', ($(this).data('x')*100)+'%');
	});
	
	if($('.bikeOption').length > 0){
		$('#bike1, #bike2').select2().maximizeSelect2Height({
			cushion: 285 // Must be a numerical pixel value.
		});
		$('.bikeOption').select2({
			placeholder: "PILIH MOTOR",
			minimumResultsForSearch: Infinity
		});
	}
	if($('.categoryMobile').length > 0){
		$('.categoryMobile').select2({
			containerCssClass: "categoryDDown",
			minimumResultsForSearch: Infinity
		});
	}

	// New Popup
	$('.new-popup-open').click(function() {
		var popupID = $(this).attr('href');
		$(popupID).fadeIn();
		$('body, html').css('overflow','hidden');
		return false;
	});

	$('.new-popup-close, .new-popup-overlay').click(function() {
		$('.new-popup').fadeOut();
		$('body, html').css('overflow','visible');
	});
})

if ( $('.articleBox').length > 0 ) {
  $.ajax({
   type: 'GET',
   url: 'http://www.astra-honda.com/racing/api/news',
   dataType: 'json',
   error: function(xhr, error){
    console.log(xhr);
    console.log(error);;
   },
   success: function(result) {
    var titleFirst = result.data[0].title;
    var linkFirst = result.data[0].link;
    var imageFirst = result.image_path + result.data[0].image;

    var dateFirst = new Date(result.data[0].date);
    var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];

    var dateNumFirst = dateFirst.getDate();
    var dateMonthFirst = months[parseInt(dateFirst.getMonth(), 10)];
    var dateYearFirst = dateFirst.getFullYear();
    // console.log(dateNumFirst, dateMonthFirst, dateYearFirst);

    $('#bp2.bannerPanel').html('<div class="overlay"></div><a href="http://www.astra-honda.com/racing/" class="meta-cat">Racing</a><div class="post-title"><a href="' + linkFirst + '" class="readmore"><h3>' + titleFirst + '</h3></a></div><div class="datering"><span class="datena">' + dateNumFirst + '</span><span class="datenb">' + dateMonthFirst + '<br>' + dateYearFirst + '</span></div><img src="' + imageFirst + '">');

    var total = result.total;
    $('.home_racing .cat_post_count span').text(total);

    $('.articleBox').html('');
    for (var i in result.data) {
     var title = result.data[i].title;
     var link = result.data[i].link;
     var image = result.image_path + result.data[i].image;
     // console.log(title, link, image);

     $('.articleBox').append('<div class="articlePanel fullBox"><div class="thumb"><div class="cats_and_formats_box"><a href="' + link + '">Racing</a><div class="post_format"></div></div><img src="' + image + '"></div><div class="racing_info"><h2><a href="' + link + '">' + title + '</a></h2></div></div>');
    }
   }
  });
 }

 var slideRacing = $('.slide_racing .owl-carousel');
 slideRacing.owlCarousel({
  items:1,
  autoplay:true,
  autoplayHoverPause:true,
  loop:true,
  nav:true,
  navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
 });

 if ( $('.slide_racing').length > 0 ) {
  $.ajax({
   type: 'GET',
   url: 'http://www.astra-honda.com/racing/api/banner',
   dataType: 'json',
   error: function(xhr, error){
    console.log(xhr);
    console.log(error);;
   },
   success: function(result) {
    $('.slide_racing .owl-carousel').html('');
    for (var i in result.data) {
     var title = result.data[i].title;
     var image = result.image_path + result.data[i].image;
     $('.slide_racing .owl-carousel').append('<div><img src="' + image + '" alt="' + title + '" title="' + title + '"></div>');
    }

    slideRacing.owlCarousel('destroy');
    slideRacing.owlCarousel({
     items:1,
     autoplay:true,
     autoplayHoverPause:true,
     loop:true,
     nav:true,
     navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
    });
   }
  });
 }

 if ( $('#bp1.bannerPanel').length > 0 ) {
  $.ajax({
   type: 'GET',
   url: 'http://www.astra-honda.com/safety-riding/api/news',
   dataType: 'json',
   error: function(xhr, error){
    console.log(xhr);
    console.log(error);;
   },
   success: function(result) {
    var titleFirst = result.data.title;
    var linkFirst = result.data.link;
    var imageFirst = result.data.image;

    var dateFirst = new Date(result.data.date);
    var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];

    var dateNumFirst = dateFirst.getDate();
    var dateMonthFirst = months[parseInt(dateFirst.getMonth(), 10)];
    var dateYearFirst = dateFirst.getFullYear();
    // console.log(dateNumFirst, dateMonthFirst, dateYearFirst);

    $('#bp1.bannerPanel').html('<div class="overlay"></div><a href="http://www.astra-honda.com/racing/" class="meta-cat">safety-riding</a><div class="post-title"><a href="' + linkFirst + '" class="readmore"><h3>' + titleFirst + '</h3></a></div><div class="datering"><span class="datena">' + dateNumFirst + '</span><span class="datenb">' + dateMonthFirst + '<br>' + dateYearFirst + '</span></div><img src="' + imageFirst + '">');
   }
  });
 }
 

$(window).bind('scroll', function () {
    if ($(window).scrollTop() > $('header').height()) {
        $('nav').addClass('fixed');
		$('.backtopScroll').css({
			right:15
		});
    } else {
        $('nav').removeClass('fixed');
		$('.backtopScroll').css({
			right:-80
		});
    }
	if ($(".profileDealers").length > 0){
		if ($(window).scrollTop() > $('.profileDealers').offset().top) {
			$('.visionMission .headline .left, .visionMission .headline .right').addClass('fadeInUp');
		}
		if ($(window).scrollTop() > $('.profileDealers').offset().top + 200) {
			$('.oneheartLogo').addClass('zoomIn');
		}
	}
	if ($(".awards").length > 0){
		if ($(window).scrollTop() > $('.awards').offset().top - 700) {
			$('.awardsPanel').addClass('zoomIn');
		}
	}
});
