function show_promo(){
	$('.ahm-popupwrapper, .ahm-popupbanner').fadeIn();
	$('body, html').css('overlay','hidden');
}
function hide_promo(){
	$('.ahm-popupwrapper, .ahm-popupbanner').fadeOut();
	$('body, html').css('overlay','auto');
}
$(document).ready(function(){
	/*$('.ahm-menutabs li a').click(function(){
		if($(this).hasClass('active')){
		}
		else{
			var gethref = $(this).attr('href');
			$('.ahm-menutabs li a').removeClass('active');
			$(this).addClass('active');
			$('.ahm-menu-content').hide();
			$(gethref).fadeIn();
		}
		return false;
	});*/
	$('.ahm-menutabs li a').removeClass('active');
	if($('.ahm-maintabs').length>0){
		$('.ahm-maintabs').easytabs();
	}
	if($('.ahm-banner-po').length>0){
		$(".ahm-banner-po").owlCarousel({
			items:1,
			autoplay:true,
			autoplayHoverPause:true,
			loop:true,
			nav:true,
			navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
		});
	}
	/*$('.tab-mobile').click(function(){
		if($(this).hasClass('active')){
		}
		else{
			var gethref2 = $(this).attr('href');
			$('.tab-mobile').removeClass('active');
			$(this).addClass('active');
			$('.ahm-menu-content').slideUp().fadeOut();
			$(gethref2).slideDown();
		}
		return false;
	});*/
	//var showtab = $('.ahm-menutabs li:first-child a').attr('href');
	//$(showtab).show();
	$('.ahm-title').addClass('ahm-animation-zoom-in');
});